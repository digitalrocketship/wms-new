const data = [
  {
    container_receipt: '519751 MIT',
    desc: '3ml VTM w/LARYNGOPHARYNGEAL FLOCKED SWABS, 400pcs. UOM:400',
    hawb: 'CNSZX160529',
    part_no: 'MIT.UTM-B',
    lot_no: '2020100100',
    serial_no: '',
    date: '02 November 2020',
    volume: '0.06627',
    qty: 340
  },
  {
    container_receipt: '519751 MIT',
    desc: '3ml VTM w/LARYNGOPHARYNGEAL FLOCKED SWABS, 400pcs. UOM:400',
    hawb: 'CNSZX160529',
    part_no: 'CED.XPRSARS-COV2-10',
    lot_no: '1000220916',
    serial_no: '',
    date: '02 November 2020',
    volume: '0.06627',
    qty: 55
  },
  {
    container_receipt: '519751 MIT',
    desc: '3ml VTM w/LARYNGOPHARYNGEAL FLOCKED SWABS, 400pcs. UOM:400',
    hawb: 'CNSZX160529',
    part_no: 'MIT.UTM-B',
    lot_no: '2020100100',
    serial_no: '',
    date: '02 November 2020',
    volume: '0.06627',
    qty: 600
  }
]

export default data;