const departmentData = [
  {
    delete: false,
    department_id: '',
    department_name: 'Department #1',
    contact_persons: [
      {
        id: '',
        delete: false,
        name: 'John Doe',
        first_name: 'John',
        last_name: 'Doe',
        contact_no: '12312331',
        email: 'test1@email.com'
      },
      {
        id: '',
        delete: false,
        name: 'Mary Jane',
        first_name: 'Mary',
        last_name: 'Jane',
        contact_no: '9999999',
        email: 'test2@email.com'
      }
    ]
  },
  {
    delete: false,
    department_id: '',
    department_name: 'Department #2',
    contact_persons: [
      {
        id: '',
        delete: false,
        name: 'Jack Russell',
        first_name: 'Jack',
        last_name: 'Russell',
        contact_no: '12312331',
        email: 'test3@email.com'
      },
      {
        id: '',
        delete: false,
        name: 'Clarissa Pheung',
        first_name: 'Clarissa',
        last_name: 'Pheung',
        contact_no: '868132413',
        email: 'test4@email.com'
      }
    ]
  }
]

export default departmentData;