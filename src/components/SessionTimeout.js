import React, {useState, useEffect, useContext} from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import { AuthContext } from "./Auth/AuthDataProvider";

function IdleMonitor() {
  const [idleModal, setIdleModal] = useState(false);
  const authContext = useContext(AuthContext);

  let idleTimeout = 1000 * 60 * 10;
  let idleLogout = 1000 * 60 * 15;
  let idleEvent; 
  let idleLogoutEvent;

  const events = [
    'mousemove',
    'click',
    'keypress'
  ];

  const logOut = () => {
    authContext.clearStorage();
    window.location = "/";
  }

  const sessionTimeout = () => {  
    if (!!idleEvent) clearTimeout(idleEvent);
    if (!!idleLogoutEvent) clearTimeout(idleLogoutEvent);

    idleEvent = setTimeout(() => setIdleModal(true), idleTimeout);
    idleLogoutEvent = setTimeout(() => logOut(), idleLogout);
  };

  const extendSession = () => {
    setIdleModal(false);
  }

  useEffect(() => {
    for (let e in events) {
      window.addEventListener(events[e], sessionTimeout);
    }

    return () => {
      for(let e in events) {
        window.removeEventListener(events[e], sessionTimeout);
      }
    }
  });

  return (
    <Modal isOpen={idleModal} toggle={() => setIdleModal(false)}>
      <ModalHeader toggle={() => setIdleModal(false)}>
        You Have Been Idle!
      </ModalHeader>
      <ModalBody>
        Your session will expire in {idleLogout / 60 / 1000} minutes. Do you want to extend the session?
      </ModalBody>
      <ModalFooter>
        <button className="btn btn-info"  onClick={()=> logOut()}>Logout</button>
        <button className="btn btn-success" onClick={()=> extendSession()}>Stay</button>
      </ModalFooter>
    </Modal>
  );
}

export default IdleMonitor;