import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';

import Header from '../../layout/Header';
import BackToTop from '../../components/Header/BackToTop';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    height: '56px',
  },
  toolbar: theme.mixins.toolbar,
  toolbarHeight: {
      minHeight: '56px',
  },
  content: {
    flexGrow: 1,
    padding: 0,
    height: '56px',
  },
}));


const CreatePageLayout = ({ children, window, ...rest }) => {
  const classes = useStyles();
  
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar elevation={0} position="fixed" className={classes.appBar}>
        <Toolbar className={classes.toolbarHeight}>
            <Header />
        </Toolbar>
      </AppBar>
      <main className={classes.content}>
        <div className={classes.toolbarHeight} />
        {children}
      </main>
      <BackToTop />
    </div>
  )
}

export default CreatePageLayout;
