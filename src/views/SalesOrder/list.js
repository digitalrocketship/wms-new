import React, { Component } from "react";

import {
  Box,
  CircularProgress,
  Typography
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

import MUIDataTable, { debounceSearchRender } from "mui-datatables";
import { MuiThemeProvider } from '@material-ui/core/styles';
import TableTheme from '../../components/Table/Theme';
import Icon from "@material-ui/core/Icon";
import { ReactComponent as EditIcon } from '../../assets/img/icons/edit.svg';
import { ReactComponent as DeleteIcon } from '../../assets/img/icons/delete.svg';

import StickyPageHeader from '../../components/PageHeader/StickyPageHeader';
import BreadCrumb from "../../components/Navs/Breadcrumb";
import IconButton from "../../components/Button/IconButton";
import RemoveDialog from '../../components/Dialog/RemoveDialog';
import PreviewDialog from "./Partial/FormPreviewDialog";

import Select from 'react-select';
import { Row, Label, InputGroup, InputGroupButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Input, Card, CardBody, Col } from "reactstrap";

import moment from 'moment';

import apiUtil from "../../api/apiUtil";
import customerApi from "../../api/Customer";
import salesOrderApi from "../../api/SalesOrder";

class SalesOrderList extends Component {
  constructor(props) {
    super(props);

    this.pageName = "salesorder_list_page";
    this.customerSelectStorageSetting = "_customer_select";

    this.state = {
      tableState: {
        page: 0,
        rowsPerPage: 15,
        sortOrder: {},
        search: ''
      },
      total: 0,
      search: '',
      data: [],
      isLoading: false,
      customerList: [],
      selectedCustomer: {
        value: '0',
        name: '',
        label: ''
      },
      isFilterOpen: false,
      filter: {
        customerId: null,
        text: 'Choose..',
        field: 'query',
        value: ''
      },
      hideCustomerFilter: false,
      previewModal: false,
      selecedItem: null
    };

    this.handleCustomerChange = this.handleCustomerChange.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  changePage = (page, sortOrder, rowsPerPage) => {
    let tableState = this.state.tableState;
    tableState.page = page;
    tableState.sortOrder = sortOrder;
    tableState.rowsPerPage = rowsPerPage;

    this.setState({
      isLoading: true,
      tableState: tableState
    });

    this.getData();
  }

  sort = (page, sortOrder) => {
    let tableState = this.state.tableState;
    tableState.page = page;
    tableState.sortOrder = sortOrder;

    this.setState({
      isLoading: true,
      tableState: tableState
    });

    this.getData();
  }

  search = (page, query) => {
    let tableState = this.state.tableState;
    tableState.page = page;
    tableState.search = query;

    this.setState({
      isLoading: true,
      tableState: tableState
    });

    this.getData();
  }

  getData = async (customerId = null) => {

    if (!customerId) {
      if (!this.state.selectedCustomer.id) {
        apiUtil.toast('No customer selected.', 'error');
        return false;
      }
      customerId = this.state.selectedCustomer.id;
    }

    this.setState({ isLoading: true });

    let params = {
      customer_id: customerId,
      optional_filter_by: this.state.filter.field,
      optional_query: this.state.filter.value,
      page: this.state.tableState.page + 1,
      take: this.state.tableState.rowsPerPage,
      query: this.state.search
    };

    if (this.state.tableState.search !== '') {
      params.search = this.state.tableState.search;
    }

    await salesOrderApi.get(params).then(data => {
      this.setState({
        isLoading: false,
        data: data.data,
        total: data.total
      });
    }).catch(error => console.log(error));

  }

  getCustomer = async () => {
    await customerApi.get().then(data => {
      if (Array.isArray(data.data)) {
        let customerList = [];

        data.data.forEach((val) => {
          customerList.push({
            value: val.id,
            label: val.company_code,
            name: val.company_name
          });
        });

        this.setState({
          customerList: customerList
        });

        let customerFilter = window.localStorage.getItem('wms_customerFilter') || false;

        if (customerFilter) {
          customerFilter = JSON.parse(customerFilter);
          this.setState({
            selectedCustomer: {
              id: customerFilter.id,
              name: customerFilter.name,
              label: customerFilter.label
            }
          });
          this.getData(customerFilter.id);
        }
      } else {
        apiUtil.toast('Failed to get customer data.', 'error');
      }
    }).catch(error => console.log(error));
  }

  toggleFilter = () => {
    let isFilterOpen = this.state.isFilterOpen;

    this.setState({
      isFilterOpen: !isFilterOpen
    });
  }

  handleCustomerChange = (event) => {
    this.setState({
      selectedCustomer: {
        id: event.value,
        name: event.name,
        label: event.label
      }
    });

    window.localStorage.setItem('wms_customerFilter', JSON.stringify({
      id: event.value,
      name: event.name,
      label: event.label
    }));

    this.getData(event.value);
  }

  handleFilterChange = (event) => {
    let filter = this.state.filter;
    filter.text = event.target.innerHTML;
    filter.field = event.target.value;

    this.setState({
      filter: filter
    });
  }

  handleFilterFieldChange = (event) => {
    let filter = this.state.filter;
    filter.value = event.target.value;

    this.setState({
      filter: filter
    });
  }

  handleSearchFieldChange = (event) => {
    this.setState({
      search: event.target.value
    });
  }

  componentDidMount = () => {
    document.title = "All Sales Order | WMS";
    window.scrollTo(0, 0);

    let customer = JSON.parse(localStorage.getItem("customer"));

    if (customer) {
      this.setState({
        selectedCustomer: {
          id: customer.id,
          name: customer.company_name,
          label: customer.company_code
        },
        hideCustomerFilter: true
      }, () => {
        this.getData(customer.id);
      });
    } else {
      this.getCustomer();
    }
  }

  removeDialog = () => {
    return <RemoveDialog
      modal={this.state.modal}
      onRemove={(isRemove) => {
        if (isRemove) {
          let data = this.state.data.filter(r => r.id !== this.state.modalItem);

          salesOrderApi.delete(this.state.modalItem).then(data => {
            if (data.data.status === true) {
              apiUtil.toast(data.data.message, 'success');
            } else {
              apiUtil.toast(data.data.message, 'warning');
            }
          }).catch(err => {
            apiUtil.toast(err.message, 'error');
          });

          this.setState({
            data: data
          });
        }

        this.setState({
          modal: false,
          modalItem: null
        });
      }}
    />
  }

  addNewButton = () => {
    if (apiUtil.checkAccess("SalesOrderCreate")) {
      return <IconButton className="btn-theme btn-theme-1" label="Add New" icon="add" onClick={() => this.props.history.push('./create')} />;
    }
    else {
      return '';
    }
  }

  tableActions = (access, button) => {
    if (!access) {
      return button;
    } else if (apiUtil.checkAccess(access)) {
      return button;
    }

    return '';
  }

  deleteData = id => {
    this.setState({
      modal: true,
      modalItem: id
    });
  }

  openPreviewDialog = (rowIndex) => {
    var item = this.state.data[rowIndex];
    this.setState({
      selectedItem: item
    }, () => {
      this.setState({
        previewModal: true
      });
    });
  }

  previewDialog = () => {
    return <PreviewDialog
      modal={this.state.previewModal}
      item={this.state.selectedItem}
      onClose={() => {
        this.setState({
          previewModal: false
        });
      }}
    />
  }

  render() {
    const columns = [
      {
        name: "reference_no",
        label: "Reference No",
        options: {
          customBodyRender: (value, data) => {
            return <button type="button" className="bg-transparent border-0 text-underline p-0"
            onClick={() => this.openPreviewDialog(data.rowIndex)}>
              {value}
            </button>
          }
        }
      },
      {
        name: "handling_in_date",
        label: "Date",
        options: {
          customBodyRender: (value, data) => {
            return value ? moment(value).format('DD-MM-YYYY') : '';
          }
        }
      },
      {
        name: "",
        label: "Total Vol (m3)"
      },
      {
        name: "end_user_name",
        label: "End User"
      },
      {
        name: "hawb",
        label: "Hawb"
      },
      {
        name: "container_receipt",
        label: "CR/PR/SO"
      },
      {
        name: "id",
        label: "Action",
        options: {
          customBodyRender: (id, data) => {
            let access_token = window.localStorage.getItem("access_token");

            return <div className="btn-group d-flex align-items-center">
              {this.tableActions(null, <Tooltip title="Edit">
                <a href={'./edit?id=' + id}>
                  <EditIcon className="fix-icon-color" />
                </a>
              </Tooltip>)}
              {this.tableActions("SalesOrderDelete", <div className="btn-group d-flex align-items-center">
                <Tooltip title="Delete">
                  <button type="button" onClick={() => this.deleteData(id)} className="bg-transparent border-0">
                    <DeleteIcon />
                  </button>
                </Tooltip>
              </div>)}</div>;
          }
        }
      }
    ];

    const options = {
      selectableRows: 'none',
      rowsPerPage: this.state.tableState.rowsPerPage,
      rowsPerPageOptions: [10, 15, 50, 100],
      serverSide: true,
      count: this.state.total,
      download: false,
      print: false,
      search: false,
      onTableChange: (action, tableState) => {
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page, tableState.sortOrder, tableState.rowsPerPage);
            break;
          case 'sort':
            this.sort(tableState.page, tableState.sortOrder);
            break;
          case 'search':
            //this.search(tableState.page, tableState.searchText);
            break;
          case 'changeRowsPerPage':
            this.changePage(tableState.page, tableState.sortOrder, tableState.rowsPerPage);
            break;
          default:
          //console.log('action not handled.');
        }
      },
      downloadOptions: { filename: 'Sales Order - ' + this.state.selectedCustomer.name + '.csv', separator: ',' },
      onDownload: (buildHead, buildBody, columns, data) => {
        console.log(data);
        //data = this.state.downloadData;
        columns.splice(-1, 1);
        data.map(function (item, index) {
          item.data.splice(-1, 1);
          item.data[1] = moment(item.data[1]).format('DD-MM-YYYY');
        });
        return "\uFEFF" + buildHead(columns) + buildBody(data);
      }
    }

    const searchInput = () => {
      const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
          this.setState({
            search: event.target.value
          }, () => {
            this.search(0);
          });
        }
      }

      const onBlurHandler = (event) => {
        this.setState({
          search: event.target.value
        });
      }

      return <Input className="form-control form-input" name="searchInput" onKeyDown={handleKeyDown} onBlur={onBlurHandler} />
    }

    return <Box className="dashboard-page">
      <StickyPageHeader>
        <BreadCrumb />
        <h1 className="page-title">Sales Order</h1>
        {this.addNewButton()}
      </StickyPageHeader>

      <Card className="list-search-box">
        <CardBody>
          <Row>
            <Col md={{ size: 6, offset: 2 }}>
              <div className={this.state.hideCustomerFilter ? "d-none" : "clearfix"}>
                <Label className="control-label">
                  Choose Company Code*
                </Label>
                <Select value={this.state.selectedCustomer} className="form-input has-margin no-border" options={this.state.customerList} onChange={(e) => this.handleCustomerChange(e)} />
              </div>

              <div className="clearfix">
                <Label className="control-label">
                  Company Name
                </Label>
                <Input className="form-control has-margin form-input" defaultValue={this.state.selectedCustomer.name} disabled="" readOnly="readonly" />
              </div>

              <div className="clearfix">
                <Label className="control-label">
                  Search
                </Label>
                {/* <InputGroup className="form-input-group no-border">
                  <InputGroupButtonDropdown addonType="prepend" isOpen={this.state.isFilterOpen} toggle={this.toggleFilter}>
                    <DropdownToggle caret>{this.state.filter.text}</DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem value="query" onClick={this.handleFilterChange}>Choose..</DropdownItem>
                      <DropdownItem value="container_receipt" onClick={this.handleFilterChange}>CR/PR/SO</DropdownItem>
                      <DropdownItem value="reference_no" onClick={this.handleFilterChange}>Ref No</DropdownItem>
                      <DropdownItem value="lot_no" onClick={this.handleFilterChange}>Lot No</DropdownItem>
                      <DropdownItem value="serial_no" onClick={this.handleFilterChange}>Serial No</DropdownItem>
                      <DropdownItem value="hawb" onClick={this.handleFilterChange}>HAWB</DropdownItem>
                      <DropdownItem value="part_no" onClick={this.handleFilterChange}>Part No</DropdownItem>
                      <DropdownItem value="location" onClick={this.handleFilterChange}>Location</DropdownItem>
                    </DropdownMenu>
                  </InputGroupButtonDropdown>
                  <Input placeholder="optional.." onChange={this.handleFilterFieldChange} />
                </InputGroup> */}
                {/* <Button
                  className="btn-theme form-input-btn mt-2 mt-md-0"
                  onClick={() => this.search(0, '')}
                  color="primary"
                  variant="contained">Search</Button> */}
                {searchInput()}
              </div>

              <div className="clearfix">
                <Label className="control-label">{' '}</Label>
                <Button
                  className="btn-theme mt-2 mt-md-0"
                  onClick={() => this.search(0)}
                  color="primary"
                  variant="contained">Search</Button>
              </div>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <Card className="pt-0">
        <CardBody>
          <Row>
            <Col md="12">
              <MuiThemeProvider theme={TableTheme}>
                <MUIDataTable
                  title={
                    <Typography variant="h6">
                      Sales Order
                      {this.state.isLoading && <CircularProgress size={24} style={{ marginLeft: 15, position: 'relative', top: 4 }} />}
                    </Typography>}
                  data={this.state.data}
                  columns={columns}
                  options={options}
                />
              </MuiThemeProvider>
            </Col>
          </Row>
        </CardBody>
      </Card>
      {this.removeDialog()}
      {this.previewDialog()}
    </Box>;
  }
}

export default SalesOrderList;