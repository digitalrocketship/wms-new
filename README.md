---------------------------------------------
TaskkMe
---------------------------------------------
Theme:
https://material-ui.com/components/box/
---------------------------------------------
GIT:
https://bitbucket.org/moovaz/worknode-ui/src/master/
---------------------------------------------
XD:
https://xd.adobe.com/view/6de17c6f-7460-4cfc-98e1-60a23f108e7c-38b2/
---------------------------------------------
API:
https://authappapi.docs.apiary.io/#
https://wmsapi3.docs.apiary.io/#
https://lds4.docs.apiary.io/#


http://auth.taskk.me/login - old site

staging website and deploy cmd
url: https://wms-moovaz--dev-wms-w69w4iu8.web.app
cmd: npm run build:staging
cmd: firebase hosting:channel:deploy dev-wms